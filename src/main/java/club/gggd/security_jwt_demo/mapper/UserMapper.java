package club.gggd.security_jwt_demo.mapper;

import club.gggd.security_jwt_demo.pojo.dto.UserPermission;
import club.gggd.security_jwt_demo.pojo.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description
 * @Author srx
 * @date 2023/12/9 14:38
 */
@Mapper
public interface UserMapper extends BaseMapper<SysUser> {

    /**
     * 获取用户权限
     * @param userId
     * @return
     */
    List<UserPermission> getUserPermissionList(Integer userId);

}
