package club.gggd.security_jwt_demo.mapper;

import club.gggd.security_jwt_demo.pojo.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description
 * @Author srx
 * @date 2023/12/9 15:12
 */
@Mapper
public interface PermissionMapper extends BaseMapper<SysPermission> {
}
