package club.gggd.security_jwt_demo.mapper;

import club.gggd.security_jwt_demo.pojo.dto.UserRoleDto;
import club.gggd.security_jwt_demo.pojo.entity.SysRole;
import club.gggd.security_jwt_demo.pojo.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2023/12/9 15:04
 */
@Mapper
public interface RoleMapper extends BaseMapper<SysRole> {

    /**
     * 获取用户全部角色
     * @param userId
     * @return
     */
    List<UserRoleDto> getUserRoleList(Integer userId);
}
