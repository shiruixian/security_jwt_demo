package club.gggd.security_jwt_demo.handle;

import club.gggd.security_jwt_demo.pojo.module.BusinessException;
import club.gggd.security_jwt_demo.pojo.module.ResultCode;
import club.gggd.security_jwt_demo.pojo.module.WebResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description 全局异常处理类
 * @Author srx
 * @date 2022/12/30 14:40
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandle {

    /**
     * 拦截业务异常
     * @param e
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public WebResult handlerBusinessException(Exception e, HttpServletRequest request, HttpServletResponse response) {
        log.error("业务异常信息：{}", e.getMessage());
        e.printStackTrace();
        // 不同异常返回不同状态码
        WebResult result = WebResult.error(((BusinessException) e).getCode(), e.getMessage());
        return result;
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public WebResult handleMethodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request, HttpServletResponse response) {
        log.error("请求参数异常：{}", e.getMessage());
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        // 不同异常返回不同状态码
        WebResult result = WebResult.error(ResultCode.BAD_REQUEST.getCode(), allErrors.get(0).getDefaultMessage());
        return result;
    }

    /**
     * 处理其他异常
     * @param e
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public WebResult handleException(Exception e, HttpServletRequest request, HttpServletResponse response){
        log.error("根异常信息：{}", e.getMessage());
        e.printStackTrace();
        // 不同异常返回不同状态码
        WebResult result = WebResult.error(ResultCode.SERVER_ERROR.getCode(), ResultCode.SERVER_ERROR.getMessage());
        return result;
    }

    /**
     * 解决被全局异常捕获的问题
     * @param e
     * @throws AccessDeniedException
     */
    @ExceptionHandler(AccessDeniedException.class)
    public void accessDeniedException(AccessDeniedException e) throws AccessDeniedException {
        throw e;
    }

}
