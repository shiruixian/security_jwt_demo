package club.gggd.security_jwt_demo.handle;

import club.gggd.security_jwt_demo.service.UserService;
import club.gggd.security_jwt_demo.util.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description 自定义权限，默认admin全部允许，这里会实时获取用户最新权限
 * @Author srx
 * @date 2023/12/10 21:08
 */
@Service(value = "el")
public class ElPermissionHandle {

    @Autowired
    private UserService userService;

    @Autowired
    private UserProvider userProvider;

    public Boolean check(String ...permissions){
        // 获取当前用户的所有权限
        Integer userId = userProvider.getUserId();
        List<String> list = userService.getAuthList(userId).stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        return list.contains("admin") || Arrays.stream(permissions).anyMatch(list::contains);
    }

}
