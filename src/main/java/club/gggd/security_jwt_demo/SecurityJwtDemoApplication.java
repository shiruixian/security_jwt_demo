package club.gggd.security_jwt_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 施瑞贤
 */
@SpringBootApplication
public class SecurityJwtDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityJwtDemoApplication.class, args);
    }

}
