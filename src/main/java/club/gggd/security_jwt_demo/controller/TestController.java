package club.gggd.security_jwt_demo.controller;

import club.gggd.security_jwt_demo.mapper.UserMapper;
import club.gggd.security_jwt_demo.pojo.dto.UserPermission;
import club.gggd.security_jwt_demo.pojo.module.WebResult;
import club.gggd.security_jwt_demo.util.RedisUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description
 * @Author srx
 * @date 2023/12/9 14:39
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisUtil redisUtil;

    @GetMapping
    public WebResult test() {
        List<UserPermission> userPermissionList = userMapper.getUserPermissionList(1);
        return WebResult.success(userPermissionList);
    }

    @GetMapping("/redis")
    public WebResult redisTest() {
        Object o = redisUtil.get("user:info:1");
        Object parse = JSON.parse((String) o);
        return WebResult.success(parse);
    }
}
