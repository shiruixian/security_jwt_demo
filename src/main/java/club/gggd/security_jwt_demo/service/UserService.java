package club.gggd.security_jwt_demo.service;

import club.gggd.security_jwt_demo.pojo.entity.SysUser;
import club.gggd.security_jwt_demo.pojo.vo.LoginVo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2023/12/9 14:38
 */
public interface UserService extends IService<SysUser> {

    /**
     * 登录
     * @param vo
     * @return
     */
    String login(LoginVo vo);

    /**
     * 根据账号获取用户信息
     * @param account
     * @return
     */
    SysUser getUserInfo(String account);

    /**
     * 获取用户权限
     * @param userId
     * @return
     */
    List<SimpleGrantedAuthority> getAuthList(Integer userId);

    /**
     * 根据账号获取用户
     * @param account
     * @return
     */
    SysUser getUserByAccount(String account);

}
