package club.gggd.security_jwt_demo.pojo.entity;

import club.gggd.security_jwt_demo.pojo.module.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description
 * @Author srx
 * @date 2023/12/9 15:10
 */
@Data
@TableName("sys_permission")
public class SysPermission extends BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("permission_code")
    private String permissionCode;

    @TableField("permission_name")
    private String permissionName;

}
