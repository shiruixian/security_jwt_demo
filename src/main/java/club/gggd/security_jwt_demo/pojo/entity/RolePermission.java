package club.gggd.security_jwt_demo.pojo.entity;

import club.gggd.security_jwt_demo.pojo.module.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description
 * @Author srx
 * @date 2023/12/9 15:17
 */
@Data
@TableName("sys_role_permission_relation")
public class RolePermission extends BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("role_id")
    private Integer roleId;

    @TableField("permission_id")
    private Integer permissionId;

}
