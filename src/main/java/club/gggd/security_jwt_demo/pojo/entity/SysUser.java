package club.gggd.security_jwt_demo.pojo.entity;

import club.gggd.security_jwt_demo.pojo.module.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Description
 * @Author srx
 * @date 2023/12/9 14:34
 */
@Data
@TableName("sys_user")
public class SysUser extends BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("account")
    private String account;

    @TableField("user_name")
    private String userName;

    @TableField("password")
    private String password;

    @TableField("last_login_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;

    @TableField("enabled")
    private Integer enabled;

    @TableField("account_not_expired")
    private Integer accountNotExpired;

    @TableField("account_not_locked")
    private Integer accountNotLocked;

}
