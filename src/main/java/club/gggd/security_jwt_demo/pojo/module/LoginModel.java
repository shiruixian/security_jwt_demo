package club.gggd.security_jwt_demo.pojo.module;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Author:Lzq
 * @Date:2023/12/14
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum LoginModel {

    ALONE("alone", "单设备登录"),
    MULTI("multi", "多设备登录"),
    SAME_DEVICE_EXCLUSION("same-device-exclusion", "同端互斥登录");

    private String value;
    private String desc;

}
