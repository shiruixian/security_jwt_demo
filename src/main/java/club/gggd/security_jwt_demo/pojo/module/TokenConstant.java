package club.gggd.security_jwt_demo.pojo.module;

import lombok.Data;

/**
 * @Author:Lzq
 * @Date:2023/12/14
 */
@Data
public class TokenConstant {

    public static final String TOKEN = "login:token:";

    public static final String SESSION = "login:session:";

}
