package club.gggd.security_jwt_demo.pojo.module;

import lombok.Data;

/**
 * @Author:Lzq
 * @Date:2023/12/11
 */
@Data
public class TokenException extends RuntimeException{

    public TokenException(String message) {
        super(message);
    }

    public TokenException(ResultCode resultCode) {
        super(resultCode.getMessage());
    }
}
