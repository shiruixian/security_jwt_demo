package club.gggd.security_jwt_demo.pojo.module;

import lombok.Data;

import java.util.List;

/**
 * @Author:Lzq
 * @Date:2023/12/14
 */
@Data
public class TokenInfo {

    /**
     * key的值
     */
    private String id;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * token集合
     */
    private List<TokenSign> tokenSignList;

}
