package club.gggd.security_jwt_demo.pojo.module;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author:Lzq
 * @Date:2023/12/14
 */
@Getter
@AllArgsConstructor
public enum DeviceType {

    DEFAULT("DEFAULT", "默认设备"),
    ANDROID("ANDROID", "安卓设备"),
    IOS("IOS", "苹果设备"),
    PC("PC", "Windows设备");

    private String value;

    private String desc;

}
