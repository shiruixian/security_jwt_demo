package club.gggd.security_jwt_demo.pojo.module;

import lombok.Data;

/**
 * @Author:Lzq
 * @Date:2023/12/14
 */
@Data
public class TokenSign {

    /**
     * token
     */
    private String token;

    /**
     * 设备
     */
    private String device;

    /**
     * 有效截止期
     */
    private Long deadline;

}
