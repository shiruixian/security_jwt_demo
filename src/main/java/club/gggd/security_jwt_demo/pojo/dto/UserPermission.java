package club.gggd.security_jwt_demo.pojo.dto;

import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Description
 * @Author srx
 * @date 2023/12/9 15:26
 */
@Data
@Alias("UserPermission")
public class UserPermission {

    private Integer userId;

    private String account;

    private String permissionCode;

}
