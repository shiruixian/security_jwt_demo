package club.gggd.security_jwt_demo.pojo.dto;

import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Description
 * @Author srx
 * @date 2023/12/10 20:59
 */
@Data
@Alias("UserRoleDto")
public class UserRoleDto {

    private Integer userId;

    private String roleCode;

}
