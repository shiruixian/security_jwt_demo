package club.gggd.security_jwt_demo.pojo.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description
 * @Author srx
 * @date 2023/12/10 13:36
 */
@Data
public class LoginVo {

    @NotBlank(message = "账号不能为空")
    private String account;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "验证码不能为空")
    private String code;

}
